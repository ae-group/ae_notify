<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.92 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.13 -->
# notify 0.3.5

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_notify/develop?logo=python)](
    https://gitlab.com/ae-group/ae_notify)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_notify/release0.3.3?logo=python)](
    https://gitlab.com/ae-group/ae_notify/-/tree/release0.3.3)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_notify)](
    https://pypi.org/project/ae-notify/#history)

>ae_notify module 0.3.5.

[![Coverage](https://ae-group.gitlab.io/ae_notify/coverage.svg)](
    https://ae-group.gitlab.io/ae_notify/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_notify/mypy.svg)](
    https://ae-group.gitlab.io/ae_notify/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_notify/pylint.svg)](
    https://ae-group.gitlab.io/ae_notify/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_notify)](
    https://gitlab.com/ae-group/ae_notify/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_notify)](
    https://gitlab.com/ae-group/ae_notify/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_notify)](
    https://gitlab.com/ae-group/ae_notify/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_notify)](
    https://pypi.org/project/ae-notify/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_notify)](
    https://gitlab.com/ae-group/ae_notify/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_notify)](
    https://libraries.io/pypi/ae-notify)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_notify)](
    https://pypi.org/project/ae-notify/#files)


## installation


execute the following command to install the
ae.notify module
in the currently active virtual environment:
 
```shell script
pip install ae-notify
```

if you want to contribute to this portion then first fork
[the ae_notify repository at GitLab](
https://gitlab.com/ae-group/ae_notify "ae.notify code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_notify):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_notify/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.notify.html
"ae_notify documentation").
